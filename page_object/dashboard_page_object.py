from selenium.webdriver.common.by import By

from page_object.base_page_object import BasePageObject


class DashboardPageObject(BasePageObject):
    URL = 'https://opensource-demo.orangehrmlive.com/index.php/dashboard'
    DASHBOARD_HEAD = (By.CLASS_NAME, 'head')
    USER_PANEL = (By.ID, 'welcome')
    LOGOUT_BUTTON = (By.PARTIAL_LINK_TEXT, 'Logout')

    def confirm_is_current_page(self):
        dashboard_header = self.wait_for_element_to_be_visible(*self.DASHBOARD_HEAD)
        assert dashboard_header.text == 'Dashboard'

    def log_out(self):
        user_panel = self.driver.find_element(*self.USER_PANEL)
        user_panel.click()

        logout_button = self.wait_for_element_to_be_clickable(*self.LOGOUT_BUTTON)
        logout_button.click()

        return self.get_login_page()

    def get_login_page(self):
        from page_object.login_page_object import LoginPageObject

        return LoginPageObject(self.driver)
