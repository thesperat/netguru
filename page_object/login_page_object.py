from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


from page_object.base_page_object import BasePageObject


class LoginPageObject(BasePageObject):
    URL = 'https://opensource-demo.orangehrmlive.com/index.php/auth/login'
    LOGIN_BOX = (By.ID, 'txtUsername')
    PASSWORD_BOX = (By.ID, 'txtPassword')
    LOGIN_BUTTON = (By.ID, 'btnLogin')
    INPUT_ERROR_MESSAGE = (By.ID, 'spanMessage')
    ERROR_MESSAGES = {
        'credentials': 'Invalid credentials',
        'empty_username': 'Username cannot be empty',
        'empty_password': 'Password cannot be empty'
    }

    def confirm_is_current_page(self):
        self.wait_for_element_to_be_clickable(*self.LOGIN_BOX)

    def find_input_boxes(self):
        login_input_box = self.driver.find_element(*self.LOGIN_BOX)
        password_input_box = self.driver.find_element(*self.PASSWORD_BOX)
        return login_input_box, password_input_box

    def insert_credentials_into_input_boxes(self, username, password):
        login_input_box, password_input_box = self.find_input_boxes()
        login_input_box.clear()
        login_input_box.send_keys(username)
        password_input_box.clear()
        password_input_box.send_keys(password)

    def click_login_button(self):
        login_button = self.driver.find_element(*self.LOGIN_BUTTON)
        login_button.click()

    def log_in(self, username, password):
        self.insert_credentials_into_input_boxes(username, password)
        self.click_login_button()
        return self.get_dashboard_page()

    def log_in_with_enter_key(self, username, password):
        _, password_input_box = self.find_input_boxes()
        self.insert_credentials_into_input_boxes(username, password)
        password_input_box.send_keys(Keys.ENTER)
        return self.get_dashboard_page()

    def try_to_log_in_with_bad_credentials(self, username, password):
        self.insert_credentials_into_input_boxes(username, password)
        self.click_login_button()
        self.wait_for_error_message(self.ERROR_MESSAGES['credentials'])

    def try_to_login_with_only_username(self, username, password=''):
        self.insert_credentials_into_input_boxes(username, password)
        self.click_login_button()
        self.wait_for_error_message(self.ERROR_MESSAGES['empty_password'])

    def try_to_login_with_only_password(self, password, username=''):
        self.insert_credentials_into_input_boxes(username, password)
        self.click_login_button()
        self.wait_for_error_message(self.ERROR_MESSAGES['empty_username'])

    def wait_for_error_message(self, message):
        def _compare_message(driver):
            try:
                error_message = driver.find_element(*self.INPUT_ERROR_MESSAGE)
                if error_message.text == message:
                    return True
                return False
            except NoSuchElementException:
                return False

        self.wait.until(_compare_message)

    def get_dashboard_page(self):
        from page_object.dashboard_page_object import DashboardPageObject

        return DashboardPageObject(self.driver)

