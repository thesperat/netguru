from selenium.webdriver.support.ui import WebDriverWait
from utils.waits import WaitMix


class BasePageObject(WaitMix):

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 15, poll_frequency=0.1)
        self.go_to_page()
        self.confirm_is_current_page()

    def confirm_is_current_page(self):
        pass

    def go_to_page(self):
        self.driver.get(self.URL)