import pytest

from page_object.login_page_object import LoginPageObject
from page_object.dashboard_page_object import DashboardPageObject


@pytest.mark.parametrize("width,height", [(1920, 1080), (480, 900)])
def test_login_logout(selenium, width, height, login_credentials):
    selenium.set_window_size(width, height)

    # Tom wants to access dashboard page,
    # He lands on login page and provides his username and password
    # Then he hits login button, he's redirected to dashboard page
    login_page_object = LoginPageObject(selenium)
    login_page_object.log_in(*login_credentials)

    # When he tries to go base site url he's redirected to dashboard page
    selenium.get('https://opensource-demo.orangehrmlive.com/')
    dashboard_page_object = DashboardPageObject(selenium)

    # Now he wants to log out
    # He's again on login page
    dashboard_page_object.log_out()

    # He inserts base site url to ensure that he's redirected to login page
    selenium.get('https://opensource-demo.orangehrmlive.com/')
    login_page_object = LoginPageObject(selenium)

    # He provides username and password once more
    # But this time instead of clicking login button, he hits enter
    # And then he logs out again
    dashboard_page_object = login_page_object.log_in_with_enter_key(*login_credentials)
    dashboard_page_object.log_out()


@pytest.mark.parametrize("width,height", [(1920, 1080), (480, 900)])
def test_login_with_wrong_inputs(selenium, login_credentials, login_wrong_credentials, width, height):
    selenium.set_window_size(width, height)

    # Tom wants to check what will happen if he inserts wrong credentials
    # He goes to the login page and then he inserts wrong credentials
    login_page_object = LoginPageObject(selenium)
    login_page_object.try_to_log_in_with_bad_credentials(*login_wrong_credentials)

    # Then he tries to insert only username, leaving password input blank
    username, password = login_credentials
    login_page_object.try_to_login_with_only_username(username)

    # He does same thing for password input
    login_page_object.try_to_login_with_only_password(password)



