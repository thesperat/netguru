import selenium.webdriver.support.expected_conditions as EC


class WaitMix:
    def wait_for_element_to_be_visible(self, locator, selector):
        return self.wait.until(EC.visibility_of_element_located((locator, selector)))

    def wait_for_element_to_be_present(self, locator, selector):
        return self.wait.until(EC.presence_of_element_located((locator, selector)))

    def wait_for_element_to_be_clickable(self, locator, selector):
        return self.wait.until(EC.element_to_be_clickable((locator, selector)))

    def wait_for_element_to_be_invincible(self, locator, selector):
        return self.wait.until(EC.invisibility_of_element((locator, selector)))