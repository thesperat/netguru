import os
import pytest


@pytest.fixture
def chrome_options(chrome_options):
    if os.environ.get('HEADLESS') == '0':
        chrome_options.add_argument('--headless')
    return chrome_options


@pytest.fixture
def login_credentials():
    username = 'Admin'
    password = 'admin123'
    return username, password


@pytest.fixture
def login_wrong_credentials():
    username = 'WrongUsername'
    password = 'wrongPassword'
    return username, password
